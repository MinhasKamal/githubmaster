# ASCII Text Art

<div align="center">
  <a href="/ascii-text-art/christmas-tree.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269825/4caa8892-1a2d-11e6-8d48-dfc49814e1ba.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/bull.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269820/4ca2d6b0-1a2d-11e6-9371-e6405462f937.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/cows.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269823/4ca89726-1a2d-11e6-8e93-1949c0478d34.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/danger.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269822/4ca86332-1a2d-11e6-81e9-2abbd60c0169.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/dragon.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269824/4ca8eff0-1a2d-11e6-932d-8cec727c1f49.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/frog.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269826/4cbe6dbc-1a2d-11e6-89fd-30e114072557.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/ghost.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269829/4cc7ac60-1a2d-11e6-8974-256584003719.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/rocket.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269828/4cc67462-1a2d-11e6-99ba-04d4f3487484.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/rocket2.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269830/4cc8d9f0-1a2d-11e6-9f9d-6ea8257c09da.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/sword.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269827/4cc5a776-1a2d-11e6-8ad5-0370af04f7dd.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/danger2.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15335529/8e5f6fdc-1c94-11e6-9101-abdb73ec5eca.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/skull-on-fire.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15335528/8e5f5880-1c94-11e6-927b-84380b52f1c2.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/bat.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15269821/4ca48eec-1a2d-11e6-987f-e097316f234b.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/gorilla.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15335527/8e5eb286-1c94-11e6-92f8-33e94d5dc63f.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/snake.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15335530/8e613876-1c94-11e6-9d32-9aaed26249e9.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/alien-skull.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961480/ca0e5a80-2f26-11e6-8991-db98070f1625.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/alien.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961479/ca0d2cd2-2f26-11e6-941b-e662c061e837.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/monster.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961478/ca0b7a2c-2f26-11e6-9da3-35b999fbb497.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/birth-day-cake.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961482/ca109d18-2f26-11e6-961b-30859d0b0719.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/danger3.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961487/ca35103a-2f26-11e6-87c4-cee62a7e645d.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/danger4.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961481/ca0f5ad4-2f26-11e6-9b20-309d3f1f8f38.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/kangaroo.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961485/ca30e762-2f26-11e6-82b0-a3b16284b51e.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/lion.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961484/ca3061b6-2f26-11e6-8366-9693c8f33c68.PNG" height="150" width=auto style="padding:5px"/></a>
  
  <a href="/ascii-text-art/rhinoceros.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961486/ca30d9c0-2f26-11e6-898f-ab79317084fd.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/tortoise.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961489/ca406548-2f26-11e6-806f-534f03274229.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/unicorn.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961488/ca3fada6-2f26-11e6-9c1e-14347f602fa6.PNG" height="150" width=auto style="padding:5px"/></a>
  <a href="/ascii-text-art/skull.md"><img src="https://cloud.githubusercontent.com/assets/5456665/15961490/ca41f84a-2f26-11e6-9154-9f5f4923991b.PNG" height="150" width=auto style="padding:5px"/></a>
</div>

# Sources

1. http://artscene.textfiles.com/asciiart/
2. http://www.asciiworld.com/
