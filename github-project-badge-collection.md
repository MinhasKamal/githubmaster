<div align="center">
<h1>GitHub Project Badge Collection</h1>
</div>

# License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html">
  <img alt="GNU General Public License" width=200px height=auto
  src="https://cloud.githubusercontent.com/assets/5456665/14769694/4ab5a970-0a80-11e6-9b63-4e903523013b.png" />
</a>
<br/>
PROJECT is licensed under a 
<a rel="license" href="http://www.gnu.org/licenses/gpl.html">
  GNU General Public License version-3
</a>.
