<div align="center">
<h1>GitHub Emoji</h1>

| <a href="#chracters">CHARACTERS</a> 
| <a href="#faces">FACES</a> 
| <a href="#clocks">CLOCKS</a> 
| <a href="#arrows">ARROWS</a> 
| <a href="#flags">FLAGS</a> 
| <a href="#astral-substances">ASTRAL_SUBSTANCES</a> |
</div>
___

<br/><br/>

### Chracters
   |   |   |   |   |   |   |   |    
---|---|---|---|---|---|---|---|---
 :a: | `:a:` | | :b: | `:b:`| | :ab: | `:ab:` 
 :abc: | `:abc:` | | :abcd: | `:abcd:` | | :capital_abcd: | `:capital_abcd:` 
 :hash: | `:hash:`| | :symbols: | `:symbols:` | | :zero: | `:zero:` 
 :one: | `:one:` | | :two: | `:two:` | | :three: | `:three:`
 :four: | `:four:` | | :five: | `:five:` | | :six: | `:six:` 
 :seven: | `:seven:` | | :eight: | `:eight:`| | :nine: | `:nine:` 
 :keycap_ten: | `:keycap_ten:` | | :1234: | `:1234:` 
<br/>

### Faces
   |   |   |   |   |   |   |   |   
---|---|---|---|---|---|---|---|---
 :bowtie: | `:bowtie:` | | :smile: | `:smile:` | | :laughing: | `:laughing:` 
 :blush: | `:blush:` | | :smiley: | `:smiley:` | | :relaxed: | `:relaxed:` 
 :smirk: | `:smirk:` | | :heart_eyes: | `:heart_eyes:` | | :kissing_heart: | `:kissing_heart:` 
 :kissing_closed_eyes: | `:kissing_closed_eyes:` | | :flushed: | `:flushed:` | | :relieved: | `:relieved:` 
 :satisfied: | `:satisfied:` | | :grin: | `:grin:` | | :wink: | `:wink:` 
 :stuck_out_tongue_winking_eye: | `:stuck_out_tongue_winking_eye:` | | :kissing: | `:kissing:` | | :grinning: | `:grinning:` 
 :stuck_out_tongue_closed_eyes: | `:stuck_out_tongue_closed_eyes:` | | :stuck_out_tongue: | `:stuck_out_tongue:` | | :confused: | `:confused:`
 :kissing_smiling_eyes: | `:kissing_smiling_eyes:` | | :sleeping: | `:sleeping:` | | :worried: | `:worried:`
 :frowning: | `:frowning:` | | :anguished: | `:anguished:` | | :open_mouth: | `:open_mouth:`
 :disappointed_relieved: | `:disappointed_relieved:` | | :grimacing: | `:grimacing:` | | :innocent: | `:innocent:`
 :hushed: | `:hushed:` | | :expressionless: | `:expressionless:` | | :unamused: | `:unamused:`
 :sweat_smile: | `:sweat_smile:` | | :sweat: | `:sweat:` | | :no_mouth: | `:no_mouth:`
 :weary: | `:weary:` | | :pensive: | `:pensive:` | | :disappointed: | `:disappointed:`
 :confounded: | `:confounded:` | | :fearful: | `:fearful:` | | :cold_sweat: | `:cold_sweat:`
 :persevere: | `:persevere:` | | :cry: | `:cry:` | | :sob: | `:sob:`
 :joy: | `:joy:` | | :astonished: | `:astonished:` | | :scream: | `:scream:`
 :neckbeard: | `:neckbeard:` | | :tired_face: | `:tired_face:` | | :angry: | `:angry:`
 :rage: | `:rage:` | | :triumph: | `:triumph:` | | :sleepy: | `:sleepy:`
 :yum: | `:yum:` | | :mask: | `:mask:` | | :sunglasses: | `:sunglasses:`
 :dizzy_face: | `:dizzy_face:` | | :imp: | `:imp:` | | :smiling_imp: | `:smiling_imp:`
 :neutral_face: | `:neutral_face:`  | | :alien: | `:alien:` | |   |
<br/>

### Clocks
   |   |   |   |   |   |   |   |    
---|---|---|---|---|---|---|---|---
 :clock1: | `:clock1:` | | :clock130: | `:clock130:`| | :clock2: | `:clock2:` 
 :clock230: | `:clock230:` | | :clock3: | `:clock3:` | | :clock330: | `:clock330:` 
 :clock4: | `:clock4:` | | :clock430: | `:clock430:`| | :clock5: | `:clock5:` 
 :clock530: | `:clock530:` | | :clock6: | `:clock6:` | | :clock630: | `:clock630:` 
 :clock7: | `:clock7:` | | :clock730: | `:clock730:`| | :clock8: | `:clock8:` 
 :clock830: | `:clock830:` | | :clock9: | `:clock9:` | | :clock930: | `:clock930:` 
 :clock10: | `:clock10:` | | :clock1030: | `:clock1030:`| | :clock11: | `:clock11:` 
 :clock1130: | `:clock1130:` | | :clock12: | `:clock12:` | | :clock1230: | `:clock1230:`
<br/>

### Arrows
   |   |   |   |   |   |   |   |    
---|---|---|---|---|---|---|---|---
 :arrow_backward: | `:arrow_backward:` | |  :arrow_forward: | `:arrow_forwar:`| | :arrow_down_small: | `:arrow_down_small:` 
 :arrow_up_small: | `:arrow_up_small:` | | :fast_forward: | `:fast_forward:` | | :rewind: | `:rewind:` 
 :arrow_double_up: | `:arrow_double_up:` | | :arrow_double_down: | `:arrow_double_down:`| | :arrow_right: | `:arrow_right:` 
 :arrow_left: | `:arrow_left:` | | :arrow_up: | `:arrow_up:` | | :arrow_down: | `:arrow_down:` 
 :arrow_lower_left: | `:arrow_lower_left:` | | :arrow_upper_left: | `:arrow_upper_left:`| | :arrow_lower_right: | `:arrow_lower_right:` 
 :arrow_upper_right: | `:arrow_upper_right:` | | :arrow_heading_down: | `:arrow_heading_down:` | | :leftwards_arrow_with_hook: | `:leftwards_arrow_with_hook:` 
 :arrow_heading_up: | `:arrow_heading_up:` | | :arrow_right_hook: | `:arrow_right_hook:`| | :arrows_counterclockwise: | `:arrows_counterclockwise:` 
 :left_right_arrow: | `:left_right_arrow:` | | :arrow_up_down: | `:arrow_up_down:` | | :twisted_rightwards_arrows: | `:twisted_rightwards_arrows:`
 :repeat: | `:repeat:` | | :repeat_one: | `:repeat_one:` | |  | 
<br/>

### Flags
   |   |   |   |   |   |   |   |    
---|---|---|---|---|---|---|---|---
 :triangular_flag_on_post: | `:triangular_flag_on_post:` | |  :checkered_flag: | `:checkered_flag:`| | :crossed_flags: | `:crossed_flags:` 
 :flags: | `:flags:` | | :jp: | `:jp:`| | :kr: | `:kr:` 
 :us: | `:us:` | | :cn: | `:cn:` | | :fr: | `:fr:` 
 :es: | `:es:` | | :it: | `:it:`| | :ru: | `:ru:` 
 :gb: | `:gb:` | | :uk: | `:uk:` | | :de: | `:de:` 
<br/>

### Astral Substances 
   |   |   |   |   |   |   |   |    
---|---|---|---|---|---|---|---|---
 :earth_asia: | `:earth_asia:` | |  :earth_africa: | `:earth_africa:`| | :earth_americas: | `:earth_americas:` 
 :globe_with_meridians: | `:globe_with_meridians:` | | :milky_way: | `:milky_way:`| | :sun_with_face: | `:sun_with_face:` 
 :full_moon_with_face: | `:full_moon_with_face:` | | :new_moon_with_face: | `:new_moon_with_face:` | | :new_moon: | `:new_moon:` 
 :waxing_crescent_moon: | `:waxing_crescent_moon:` | | :first_quarter_moon: | `:first_quarter_moon:`| | :waxing_gibbous_moon: | `:waxing_gibbous_moon:` 
 :full_moon: | `:full_moon:` | | :waning_gibbous_moon: | `:waning_gibbous_moon:` | | :last_quarter_moon_with_face: | `:last_quarter_moon_with_face:` 
 :waning_crescent_moon: | `:waning_crescent_moon:` | | :last_quarter_moon: | `:last_quarter_moon:` | | :first_quarter_moon_with_face: | `:first_quarter_moon_with_face:` 
 :crescent_moon: | `:crescent_moon:` | |  |  | |  | 
<br/>


# References

1. http://www.emoji-cheat-sheet.com/
2. https://github.com/scotch-io/All-Github-Emoji-Icons
