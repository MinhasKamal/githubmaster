<div align="center">
<h1>GitHub Readme Template</h1>
</div>

<br/><br/>

# Project Title

Describe briefly about your project. You may also provide project's website link, badges, community & contact info (i.e. 
email, social site).

# Download

Runnable file (executable or minified or installation file) link. There can be links to previous versions too.

# Installation

How your work can be used. It may include the prerequisites, settings, third party libraries, usage, cautions, etc.

# Demonstration

It may include code sample, gif file, video link, or even screen shots.

# Authors

Author names, contact info, etc.

# Acknowledgments

List of people or community helped and inspired throughout the project

# Contributing

Instructions to contribute (i.e. add feature, report bug, submit patch) to the project. May include documentation link too.

# License

Give a short intro over your license. You can give a link to the license site too.
